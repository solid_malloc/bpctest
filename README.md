# bpctest

## Requirements
- `cmake 3.11`

## Build instructions
### Build the main program
I recommend running
```
cmake -B build .
make -C build
```
on the root directory of this project. The path to the executable will be `build/bpctest`.

### Build the test libraries
Run the same commands I suggested for the main program inside the directories `test_libs/bpctest_library_1/` and `test_libs/bpctest_library_2/`, then do the same in `test_libs/bpctest_library_3/`.

## Scripting language
There are only two instructions:
- `load [library_path]` will load a shared library (`.so`) located at the given path
- `call [function_name]` will attempt to find and execute a function called `function_name`. **it will only look in whatever library was loaded inmediately before**

Comments can be placed using `#`. Text in a line after a comment will be ignored.

## Execution instructions
### Usage
The syntax to invoke the program is
```
./bpctest file1.sc
```

### Testing
There is a test1.sc file in `tests/` that showcases the basic operations. Assuming the path to the executable is `build/bpctest`, simply run
```
build/bpctest tests/test1.sc
```


## Notes

### Library pointer lifetime
The program does not close pointers to libraries once they're open until the program exits. My rationale behind this was simply that there might be some shared libraries that depend on other shared libraries. `libbpctest_library_3` is dependent from `libbpctest_library_1` and `libbpctest_library_2`. however, `libbpctest_library_3`'s compilation process writes the absolute paths to the other two inside the `.so`, and C knows to load dependencies like that automatically when loading `libbpctest_library_3`.

To test the intended use-case of this approach, one has to move the `.so` files inside the `build/` directories of `bpctest_library_1` and/or `bpctest_library_2` to another location. Then attempt to load `libbpctest_library_3` and execute `print_statement3` without first having loaded the other `.so`'s from their respective new locations.

### Function look-ups in all libraries
It would be relatively simple to perform a lookup in each loaded library for a function. Library pointers are stored in a one-way queue data structure as they are opened. One could simply walk this queue from back to front and perform a look up for each library. One could even improve the runtime performance of this action by adding a hash-table with function-name keys and library pointer values.

### Interactive mode support
`(This note assumes familiarity with the code)`

In `src/bpctest_asc.c` there is a function called `parse_line()`, whose parameters include a `FILE*`, and whose main job is simply to read until the end of a line and write to two pointers: one is a buffer for the operator (`call` or `load`) and the other points to another pointer, which gets initialized to a chunk of heap memory where we store the operand (the library path or function name). This is somewhat similar to `getline()`. In a more complex language this function would be a stack automaton, but for our purposes it should be sufficient.

Since `stdin` can be treated as a `FILE*`, one could create a while loop which calls `parse_line()` repeatedly on `stdin`, initalize the corresponding structures for `load` and `call` (pushing `load`'s into a queue for storage) and call the respective `eval` and free functions for each. In short, a mixture of the `if`'s and loop `bpctest_asc_tokenize` with the `if`'s of `bpctest_asc_eval`, with initialization and desctrution for the `load`'s queue.