cmake_minimum_required(VERSION 3.11)
project(bpctest VERSION 0.0.1)

set(CMAKE_C_STANDARD 11)
set(CMAKE_C_STANDARD_REQUIRED ON)

set(CMAKE_BUILD_TYPE Debug)

set(SOURCES 
    ${CMAKE_CURRENT_SOURCE_DIR}/src/main.c
    ${CMAKE_CURRENT_SOURCE_DIR}/src/bpctest_absop.c    
    ${CMAKE_CURRENT_SOURCE_DIR}/src/bpctest_asc.c  
    )

add_executable(bpctest ${SOURCES})

find_library(DL_LIBRARY dl)

target_include_directories(bpctest PUBLIC 
    ${CMAKE_CURRENT_SOURCE_DIR}/include
)

target_link_libraries(bpctest ${DL_LIBRARY})