cmake_minimum_required(VERSION 3.11)
project(bpctest_library_1)

set(CMAKE_BUILD_TYPE Release)

add_library(bpctest_library_1 SHARED bpctest_library_1.c)