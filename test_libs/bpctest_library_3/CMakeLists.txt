cmake_minimum_required(VERSION 3.11)
project(bpctest_library_3)

set(CMAKE_BUILD_TYPE Release)

include_directories(../bpctest_library_1)
include_directories(../bpctest_library_2)

link_directories(../bpctest_library_1/build)
link_directories(../bpctest_library_2/build)

add_library(bpctest_library_3 SHARED bpctest_library_3.c)

target_link_libraries(bpctest_library_3 bpctest_library_1 bpctest_library_2)

