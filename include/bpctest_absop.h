#ifndef BPCTEST_ABSOP_H
#define BPCTEST_ABSOP_H

// abstractions of all operators that are included in this scripting language

#include <stddef.h>

typedef void* bpctest_loadable_lib;

struct bpctest_op {
  unsigned int op_idx;

  // the only rule is one line is one command
  unsigned int line;
};

struct bpctest_op_call {
  struct bpctest_op meta;
  char* fn_name;
};

enum bpctest_op_call_codes {
  BPCTEST_OP_CALL_SUCCESS,
  BPCTEST_OP_CALL_FAIL,
};

int bpctest_op_call_eval(struct bpctest_op_call*, bpctest_loadable_lib);

struct bpctest_op_load {
  struct bpctest_op meta;
  char* lib_path;
  bpctest_loadable_lib lib;
};

enum bpctest_op_load_codes {
  BPCTEST_OP_LOAD_SUCCESS,
  BPCTEST_OP_LOAD_FAIL,
};

int bpctest_op_load_eval(struct bpctest_op_load*);
void bpctest_op_load_free(struct bpctest_op_load*);

#endif