#ifndef BPCTEST_UTILS_QUEUE_H
#define BPCTEST_UTILS_QUEUE_H
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#define BPCTEST_QUEUE_DECL(NAME_SUFFIX, TYPE)                             \
  struct bpctest_queue_##NAME_SUFFIX##_node {                             \
    struct bpctest_queue_##NAME_SUFFIX##_node *next;                      \
    TYPE data;                                                            \
  };                                                                      \
                                                                          \
  struct bpctest_queue_##NAME_SUFFIX {                                    \
    size_t size;                                                          \
    struct bpctest_queue_##NAME_SUFFIX##_node *front;                     \
    struct bpctest_queue_##NAME_SUFFIX##_node *back;                      \
  };                                                                      \
                                                                          \
  struct bpctest_queue_##NAME_SUFFIX bpctest_queue_##NAME_SUFFIX##_new(); \
                                                                          \
  void bpctest_queue_##NAME_SUFFIX##_push(                                \
      struct bpctest_queue_##NAME_SUFFIX *q, TYPE data);                  \
                                                                          \
  void bpctest_queue_##NAME_SUFFIX##_pop(                                 \
      struct bpctest_queue_##NAME_SUFFIX *q);                             \
                                                                          \
  void bpctest_queue_##NAME_SUFFIX##_free(                                \
      struct bpctest_queue_##NAME_SUFFIX *q);


// NOTE: the pop operator of this queue does not call FREE_FN. the owning scope
// of the queue must take care of potentially freeing each node's data before
// popping. adding another macro parameter could solve this, but that exceeds
// the scope of this task 
#define BPCTEST_QUEUE_IMPL(NAME_SUFFIX, TYPE, FREE_FN)                     \
  struct bpctest_queue_##NAME_SUFFIX bpctest_queue_##NAME_SUFFIX##_new() { \
    return (struct bpctest_queue_##NAME_SUFFIX){                           \
        .size = 0, .front = NULL, .back = NULL};                           \
  }                                                                        \
                                                                           \
  void bpctest_queue_##NAME_SUFFIX##_push(                                 \
      struct bpctest_queue_##NAME_SUFFIX *q, TYPE data) {                  \
    struct bpctest_queue_##NAME_SUFFIX##_node *node =                      \
        (struct bpctest_queue_##NAME_SUFFIX##_node *)malloc(               \
            sizeof(struct bpctest_queue_##NAME_SUFFIX##_node));            \
                                                                           \
    if (node == NULL) {                                                    \
      perror("bpctest_queue: error allocating new node");                  \
      exit(EXIT_FAILURE);                                                  \
    }                                                                      \
                                                                           \
    node->data = data;                                                     \
    node->next = NULL;                                                     \
    if (q->back == NULL) {                                                 \
      q->front = q->back = node;                                           \
    } else {                                                               \
      (q->back)->next = node;                                              \
      q->back = node;                                                      \
    }                                                                      \
                                                                           \
    ++q->size;                                                             \
  }                                                                        \
                                                                           \
  void bpctest_queue_##NAME_SUFFIX##_pop(                                  \
      struct bpctest_queue_##NAME_SUFFIX *q) {                             \
    if (q->front == NULL) {                                                \
      perror("bpctest_queue: error popping node");                         \
      exit(EXIT_FAILURE);                                                  \
    }                                                                      \
                                                                           \
    struct bpctest_queue_##NAME_SUFFIX##_node *front = q->front;           \
                                                                           \
    q->front = front->next;                                                \
    if (q->front == NULL) {                                                \
      q->back = NULL;                                                      \
    }                                                                      \
                                                                           \
    free(front);                                                           \
    --q->size;                                                             \
  }                                                                        \
                                                                           \
  void bpctest_queue_##NAME_SUFFIX##_free(                                 \
      struct bpctest_queue_##NAME_SUFFIX *q) {                             \
    struct bpctest_queue_##NAME_SUFFIX##_node *next_node = q->front;       \
    while (next_node != NULL) {                                            \
      struct bpctest_queue_##NAME_SUFFIX##_node *tmp = next_node->next;    \
      if (FREE_FN != NULL) {                                               \
        void (*func)(TYPE *) = FREE_FN;                                    \
        func(&(next_node->data));                                          \
      }                                                                    \
                                                                           \
      free(next_node);                                                     \
      next_node = tmp;                                                     \
    }                                                                      \
  }

#endif