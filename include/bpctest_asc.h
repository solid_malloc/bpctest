// an 'abstract syntax container', a struct to store the abstracted structs of
// each token from the input source in place of a proper AST

#ifndef BPCTEST_ASC_H
#define BPCTEST_ASC_H

#include <stdio.h>

#include "bpctest_absop.h"
#include "utils/bpctest_queue.h"

union bpctest_op_asc {
  struct bpctest_op_call call;
  struct bpctest_op_load load;
};

void bpctest_op_asc_free(union bpctest_op_asc *);

BPCTEST_QUEUE_DECL(opasc, union bpctest_op_asc)

struct bpctest_asc {
  struct bpctest_queue_opasc cmds;
};

// one could separate the tokenizer from the asc, and instead create some other
// structure to hold tokens that are then fed into a parser or parsing
// structure, but the scope here is too small for that imo
void bpctest_asc_tokenize(struct bpctest_asc *, FILE *);

enum bpctest_asc_eval {
  BPCTEST_ASC_EVAL_SUCCESS,
  BPCTEST_ASC_EVAL_FAIL
};

int bpctest_asc_eval(struct bpctest_asc *);
void bpctest_asc_free(struct bpctest_asc *);

#endif