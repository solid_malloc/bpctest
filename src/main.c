#include "bpctest_asc.h"
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {
  if (argc > 2) {
    perror("please specify only one file");
    exit(EXIT_FAILURE);
  }

  FILE *fp = fopen(argv[1], "r");

  if (fp == NULL) {
    fprintf(stderr, "error opening file %s: %s\n", argv[1], strerror(errno));
    exit(EXIT_FAILURE);
  }

  struct bpctest_asc asc = {.cmds = bpctest_queue_opasc_new()};
  bpctest_asc_tokenize(&asc, fp);

  int res = bpctest_asc_eval(&asc);
  bpctest_asc_free(&asc);
  fclose(fp);

  if (res == BPCTEST_ASC_EVAL_FAIL) {
    exit(EXIT_FAILURE);
  }

  exit(EXIT_SUCCESS);
}
