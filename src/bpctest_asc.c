#include "bpctest_asc.h"
#include "bpctest_absop.h"
#include "utils/bpctest_queue.h"
#include <ctype.h>
#include <float.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// define a size for the operator buffer, determines the maximum character
// length of an operator keyword for this parser, counting null terminator.
// it's a macro so that it doesn't whine about VLA
#define MAX_KW_LEN 4ULL

// this would be the maximum length of a line NOT counting the keyword nor the
// space character in between nor the newline
#define MAX_ARG_LEN 4096ULL

// allows a maximum of 1023 empty spaces and a null terminator
#define MAX_LINE_LEN 5124ULL

// define indexes for the operators for this particular parser
enum op_idx_asc {
  OP_CALL = 0U,
  OP_LOAD = 1U,
};

// the order here should reflect the enum
static const char OP_KEYWORDS[2][MAX_KW_LEN + sizeof('\0')] = {"call", "load"};

static const struct bpctest_op_call default_call = (struct bpctest_op_call){
    .meta = {.op_idx = OP_CALL},
    .fn_name = NULL,
};

static const struct bpctest_op_load default_load = (struct bpctest_op_load){
    .meta = {.op_idx = OP_LOAD},
    .lib_path = NULL,
    .lib = NULL,
};

BPCTEST_QUEUE_IMPL(opasc, union bpctest_op_asc, bpctest_op_asc_free)

static void skip_line(FILE *fp);

enum parse_line_codes {
  PARSE_LINE_SUCCESS,
  PARSE_LINE_FAIL,
  PARSE_LINE_END,
  PARSE_LINE_IGNORE,
};

static int parse_line(char *op_kw_buffer, char **arg_buffer, size_t *op_kw_len,
                      size_t *arg_len, FILE *fp);

void bpctest_asc_tokenize(struct bpctest_asc *asc, FILE *fp) {
  static size_t line_count = 0;

  // + 1 for the null terminator
  char op_kw_buffer[MAX_KW_LEN + 1];
  char *arg_buffer;

  size_t op_kw_len = 0;
  size_t args_len = 0;

  int parse_res =
      parse_line(op_kw_buffer, &arg_buffer, &op_kw_len, &args_len, fp);

  while (parse_res != PARSE_LINE_END) {
    ++line_count;

    if (parse_res == PARSE_LINE_FAIL) {
      fprintf(stderr,
              "bpctest_asc_tokenize error (%zu):"
              " failed to parse line\n",
              line_count);
    } else if (parse_res == PARSE_LINE_SUCCESS) {
      int is_call = strcmp(op_kw_buffer, OP_KEYWORDS[OP_CALL]) == 0;
      int is_load = strcmp(op_kw_buffer, OP_KEYWORDS[OP_LOAD]) == 0;

      // !! DEBUG !!
      // printf("parsing line: %s %s %zu %zu\n", op_kw_buffer, arg_buffer,
      //        op_kw_len, args_len);
      // !! DEBUG !!

      union bpctest_op_asc operator;

      if (!is_call && !is_load) {
        fprintf(stderr,
                "bpctest_asc_tokenize error (%zu):"
                " %s is not a valid keyword!\n",
                line_count, op_kw_buffer);

        skip_line(fp);

      } else if (is_load) {
        operator=(union bpctest_op_asc){.load = default_load};
        operator.load.lib_path = arg_buffer;
        operator.load.meta.line = line_count;

        if (args_len == 0) {
          fprintf(stderr,
                  "bpctest_asc_tokenize error (%zu):"
                  "failed to read library path\n",
                  line_count);
        }

      } else { // if (is_call)
        operator=(union bpctest_op_asc){.call = default_call};
        operator.call.fn_name = arg_buffer;
        operator.call.meta.line = line_count;

        if (args_len == 0) {
          fprintf(stderr,
                  "bpctest_asc_tokenize error (%zu):"
                  "failed to read function name\n",
                  line_count);
        }
      }

      if (args_len > 0) {
        bpctest_queue_opasc_push(&asc->cmds, operator);
      }
    }

    parse_res =
        parse_line(op_kw_buffer, &arg_buffer, &op_kw_len, &args_len, fp);
  }

  if (ferror(fp)) {
    perror("bpctest_asc_tokenize error:"
           " error reading from file\n");
    exit(EXIT_FAILURE);
  }

  // !! DEBUG !!
  // printf("read %zu lines\n", line_count);
  // !! DEBUG !!
}

static void skip_line(FILE *fp) {
  static char discard_buffer[MAX_ARG_LEN];
  while (fgets(discard_buffer, MAX_ARG_LEN, fp)) {
    if (strchr(discard_buffer, '\n')) {
      break;
    }
  }
}

static int parse_line(char *op_kw_buffer, char **arg_buffer, size_t *op_kw_len,
                      size_t *arg_len, FILE *fp) {

  static char read_buffer[MAX_LINE_LEN];
  const size_t token_max_len[2] = {MAX_KW_LEN, MAX_ARG_LEN};

  size_t curr_token = 0; // first read the keyword
  size_t curr_token_len = 0;

  if (!fgets(read_buffer, MAX_LINE_LEN, fp)) {
    return PARSE_LINE_END;
  }

  // is at most MAX_LINE_LEN
  size_t line_len = strlen(read_buffer);

  if (!feof(fp) && read_buffer[line_len - 1] != '\n') {
    fprintf(stderr,
            "parse_line error:"
            " maximum line length %llu exceeded\n",
            MAX_LINE_LEN);

    // if this line is too long, move to the next line
    skip_line(fp);

    return PARSE_LINE_FAIL;
  }

  // <= line_len to include the null terminator
  for (int i = 0; (i <= line_len) && curr_token < 2; ++i) {
    if (!isspace(read_buffer[i]) && read_buffer[i] != '\0' &&
        read_buffer[i] != '#') {
      // we found a character

      ++curr_token_len;

      // if the maximum length of the token we've been reading is exceeded,
      // ignore the rest of the line if necessary and continue
      if (curr_token_len > token_max_len[curr_token]) {
        fprintf(stderr,
                "parse_line error:"
                " maximum keyword or argument length %zu exceeded\n",
                token_max_len[curr_token]);

        return PARSE_LINE_FAIL;
      }

    } else if (curr_token_len > 0) {
      // if it's a space and we were reading

      char *token_start = read_buffer + i - curr_token_len;

      if (curr_token == 0) { // if its the keyword
        memcpy(op_kw_buffer, token_start, curr_token_len);
        op_kw_buffer[curr_token_len] = '\0';

        *op_kw_len = curr_token_len;
        curr_token_len = 0;
        ++curr_token;

      } else if (curr_token == 1) { // if its the argument
        *arg_buffer = (char *)malloc((curr_token_len + 1) * sizeof(char));
        if (*arg_buffer == NULL) {
          fprintf(stderr, "parse_line error:"
                          "memory allocation failed\n");
          exit(EXIT_FAILURE);
        }

        memcpy(*arg_buffer, token_start, curr_token_len);
        (*arg_buffer)[curr_token_len] = '\0';

        *arg_len = curr_token_len;
        curr_token_len = 0;
        ++curr_token;
      }
    }

    // if we were interrupted by a comment, ignore the rest of the line
    if (read_buffer[i] == '#') {
      break;
    }

  }

  if (curr_token == 0 && curr_token_len == 0) {
    return PARSE_LINE_IGNORE;
  }

  if (curr_token < 2) {
    fprintf(stderr, "parse_line error:"
                    " not enough tokens found\n");
    return PARSE_LINE_FAIL;
  }

  return PARSE_LINE_SUCCESS;
}

void bpctest_op_asc_free(union bpctest_op_asc *cmd) {
  struct bpctest_op *op = (struct bpctest_op *)cmd;
  if (op->op_idx == OP_CALL) {
    free(cmd->call.fn_name);
  } else if (op->op_idx == OP_LOAD) {
    free(cmd->load.lib_path);
  }
}

BPCTEST_QUEUE_DECL(op_loads, struct bpctest_op_load)
BPCTEST_QUEUE_IMPL(op_loads, struct bpctest_op_load, bpctest_op_load_free)

int bpctest_asc_eval(struct bpctest_asc *asc) {
  struct bpctest_queue_opasc *cmds_q = &asc->cmds;

  // a queue to keep track of the already loaded libraries
  struct bpctest_queue_op_loads loads_q = bpctest_queue_op_loads_new();

  while (cmds_q->size > 0) {
    union bpctest_op_asc *cmd = &((cmds_q->front)->data);
    struct bpctest_op *op = (struct bpctest_op *)cmd;

    // !! DEBUG !!
    // printf("there's %zu cmds left, next one is %u\n", asc->cmds.size,
    //        op->op_idx);
    // !! DEBUG !!

    if (op->op_idx == OP_CALL) {
      if (loads_q.size == 0) {
        fprintf(stderr, "bpctest_asc_eval error:"
                        " no libraries have been loaded\n");
        return BPCTEST_ASC_EVAL_FAIL;
      }

      bpctest_loadable_lib curr = (loads_q.back)->data.lib;
      int res = bpctest_op_call_eval(&cmd->call, curr);

      if (res == BPCTEST_OP_CALL_FAIL) {
        bpctest_queue_op_loads_free(&loads_q);
        return BPCTEST_ASC_EVAL_FAIL;
      }

      bpctest_op_asc_free(cmd);
    } else if (op->op_idx == OP_LOAD) {
      int res = bpctest_op_load_eval(&cmd->load);

      if (res == BPCTEST_OP_LOAD_FAIL) {
        bpctest_queue_op_loads_free(&loads_q);
        return BPCTEST_ASC_EVAL_FAIL;
      }
      // transfer ownership of the 'load' node to the other queue by copying it
      bpctest_queue_op_loads_push(&loads_q, cmd->load);
    }

    bpctest_queue_opasc_pop(cmds_q);
  }

  // will unload all libraries
  bpctest_queue_op_loads_free(&loads_q);

  return BPCTEST_ASC_EVAL_SUCCESS;
}

void bpctest_asc_free(struct bpctest_asc *asc) {
  bpctest_queue_opasc_free(&asc->cmds);
}