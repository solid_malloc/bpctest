#include "bpctest_absop.h"
#include <dlfcn.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

int bpctest_op_call_eval(struct bpctest_op_call *op, bpctest_loadable_lib lib) {

  printf("evaluating call op (%d): %s, \n", op->meta.line, op->fn_name);

  void (*func)() = (void (*)())dlsym(lib, op->fn_name);
  char *error = dlerror();
  if (error != NULL) {
    fprintf(stderr, "bpctest_op_call_eval error: %s\n", error);
    return BPCTEST_OP_CALL_FAIL;
  }

  func();
  return BPCTEST_OP_CALL_SUCCESS;
}

int bpctest_op_load_eval(struct bpctest_op_load *op) {
  printf("evaluating load op (%d): %s\n", op->meta.line, op->lib_path);
  op->lib = dlopen(op->lib_path, RTLD_LAZY);

  if (!op->lib) {
    fprintf(stderr, "bpctest_op_load_eval error: %s\n", dlerror());
    return BPCTEST_OP_LOAD_FAIL;
  }

  return BPCTEST_OP_LOAD_SUCCESS;
}

void bpctest_op_load_free(struct bpctest_op_load *op) {
  dlclose(op->lib);
  free(op->lib_path);
}
